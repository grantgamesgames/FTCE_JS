Point = function($x, $y)
{
	this.x = $x;
	this.y = $y;
}

Point.EMPTY = function() {
	return new Point(0, 0);
}