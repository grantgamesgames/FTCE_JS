var _junkId = 0;

function JunkEmitter(container, direction, height) {
	junkEmitter = {};
	junkEmitter.junk = [];
	junkEmitter.junkXPos = [];
	junkEmitter.junkYPos = [];
	direction = direction > 0 ? "top" : "bottom";
	junkEmitter.lastXOffset = 0;

	//generate junk
	junkEmitter.generateJunk = function(speed, minX, maxX) {
		var rand = Math.random();
		if(rand > 0.99)
		{
			container.append("<div id='junk" + (++_junkId) + "' class='junk " + direction + "'></div>");
			var junkRep = $("#junk" + _junkId);
			junkRep.css(direction, 0);
			var xPos = NumberUtils.randomRange(minX + 20, maxX - 20, 1);
			log("xPos:" + xPos);
			junkEmitter.junkXPos.push(parseInt(xPos, 10));
			junkEmitter.junk.push(junkRep);
			junkEmitter.junkYPos.push(-50);
		}
	}
	//move junk
	junkEmitter.moveJunk = function(speed, offset) {
		var currXPos;
		var currYPos;
		var junkRep;
		for(var i = junkEmitter.junk.length - 1; i >= 0; i--)
		{
			junkRep = junkEmitter.junk[i];
			currYPos = junkEmitter.junkYPos[i];
			if(direction == "top") {
				currYPos -= speed * 0.5;
			} else {
				currYPos += speed * 0.5; //bottom
			}
			if(currYPos > height)
			{
				junkRep.remove();
				junkEmitter.junkXPos.splice(i, 1);
				junkEmitter.junkYPos.splice(i, 1);
				junkEmitter.junk.splice(i, 1);
			} else {
				// log("junkEmitter.junkXPos[i]" + junkEmitter.junkXPos[i]);
				currXPos = junkEmitter.junkXPos[i] - offset;
				junkRep.css("left", currXPos + "px");

				junkEmitter.junkYPos[i] = currYPos;
				junkRep.css(direction, currYPos + "px");
			}
		}
		// log(speed);
	}

	junkEmitter.update = function(speed, offset, minX, maxX) {
		if(speed > 0)
			junkEmitter.generateJunk(speed, minX, maxX);
		junkEmitter.moveJunk(speed, offset);
		junkEmitter.lastXOffset = offset;
	}
	
	return junkEmitter;
}