//	$context = context to be drawn into;
//	$wallSize:Point = width/height
//	$blockSize:Point = width/height
//	$blockJitter:Point = widthJitter/heightJitter
//	$direction:int = -1/1
WallConfig = function($context, $wallSize, $blockSize, $blockJitter, $direction)
{
	this.context = $context;
	this.wallSize = $wallSize;
	this.blockSize = $blockSize;
	this.blockJitter = $blockJitter;
	this.direction = $direction;
}

WallConfig.WALL_FACE_LEFT = -1;
WallConfig.WALL_FACE_RIGHT = 1;