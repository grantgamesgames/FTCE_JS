// $origin:Point
// $wallConfig:WallConfig
// $wallStyle:WallStyle
Wall = function($origin, $wallConfig, $wallStyle)
{
	var wall = {};
	wall.origin = $origin;
	wall.wallConfig = $wallConfig;
	wall.wallStyle = $wallStyle;
	wall.xPoints = [];
	wall.yPoints = [];
	wall.xOffset = 0;
	wall.lastXOffset = 0;
	wall.innerEdge = 0;
	
	wall.draw = function(xOffset, yOffset) {
		wall.generatePoints(xOffset, yOffset);
		wall.createBlocks(xOffset);
	}
	
	// Updates old points and adds new ones if needed
	wall.generatePoints = function(xOffset, yOffset){};
	
	wall.createBlocks = function(xOffset){
		// Clear out old area
		var clearX;
		var clearW;
		//clear of last frames content
		if(wall.wallConfig.direction > 0)
		{
			clearX = (wall.origin.x + wall.wallConfig.blockSize.x) - wall.wallConfig.blockJitter.x;
			wall.innerEdge = clearW = wall.wallConfig.blockSize.x - wall.lastXOffset;
		} else {	
			wall.innerEdge = clearX = wall.origin.x - (wall.lastXOffset + wall.wallConfig.blockSize.x);
			clearW = wall.wallConfig.blockJitter.x;
		}
		wall.wallConfig.context.clearRect(clearX, wall.origin.y, clearW, wall.wallConfig.wallSize.y);
		
		// Draws first wall point
		wall.wallStyle.color = "#000000";
		wall.wallConfig.context.fillStyle = wall.wallStyle.color;
		wall.wallConfig.context.beginPath();
		wall.wallConfig.context.moveTo(wall.origin.x, wall.yPoints[0]); // top left
		wall.lastXOffset = xOffset;
		
		$.each(wall.yPoints, function(i, v) {
			wall.drawBlock(wall.xPoints[i], v);
		});
	
		wall.wallConfig.context.lineTo(wall.origin.x, wall.wallConfig.wallSize.y);
		// wall.wallConfig.context.lineTo(wall.origin.x, wall.origin.y);
		wall.wallConfig.context.fill();
	}
	
	wall.drawBlock = function(endX, endY) {
		wall.wallConfig.context.lineTo((wall.origin.x - wall.xOffset) + (endX * wall.wallConfig.direction), endY);
	}
	
	// Takes a set of points and offsets the y
	wall.offsetCurrentPoints = function(xOffset, yOffset) {
		wall.xOffset = xOffset;
		var xPoints = [];
		var yPoints = [];
		var newX;
		var newY;
		var yLimit = (wall.wallConfig.blockSize.y * 2) * -1;
		
		$.each(wall.yPoints, function(i, v) {
			newX = wall.xPoints[i];
			newY = v - yOffset;
			
			if(newY > yLimit) {
				xPoints.push(newX);
				yPoints.push(newY);
			}
		});
		
		return {'xPoints':xPoints, 'yPoints':yPoints};
	}
	
	
	return wall;
}