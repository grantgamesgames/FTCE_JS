function FTCE(container, width, height) {
	
	var _ftce = new Game(container, width, height);
	_ftce.fps = 50;
	var _speed = 5;
	var _wallPos = 0;
	var _xPos = 0;

	//Walls
	var _wallSize = new Point(width, height);
	var _blockSize = new Point(200, 50);
	var _blockJitter = new Point(150, 20);
	var _wallStyle = new WallStyle("0x000000");
	var _leftConfig = new WallConfig(_ftce.context, _wallSize, _blockSize, _blockJitter, WallConfig.WALL_FACE_RIGHT);
	var _leftWall = new Rock(Point.EMPTY(), _leftConfig, _wallStyle);                                            
	                                                                                                          
	var _rightOrigin = new Point(width, 0);                                                                    
	var _rightConfig = new WallConfig(_ftce.context, _wallSize, _blockSize, _blockJitter, WallConfig.WALL_FACE_LEFT);
	var _rightWall = new Rock(_rightOrigin, _rightConfig, _wallStyle);  
	
	//Arnika
	var _arnika = new Arnika(container);
	
	//Arnika's ball
	var _ball = new Ball(_arnika.representation);

	//Junk
	var _junkFromAbove = new JunkEmitter(container, 1, height);
	var _junkFromBelow = new JunkEmitter(container, -1, height);
	
	_ftce.onMove = function(e) {
		var newXPos = ((width * 0.5) - e.pageX) * 0.25;
		if(Math.abs(newXPos) <= _blockSize.x * .7)
			_xPos = newXPos;
			
		var rotation = (_xPos / _blockSize.x) * 30;
		_arnika.representation.css("webkitTransform", "rotate(" + rotation + "deg)");
		
		var arnikaPos =  -_xPos * 2;
		var currentPos = _arnika.representation.css("left");
		if(currentPos == "auto") {
			_arnika.representation.css("left", "0");
			currentPos = "0px";
		}
		currentPos = currentPos.substr(0, currentPos.length - 2);
		if(Math.abs(currentPos - arnikaPos) > 25)
			_arnika.representation.css("left", arnikaPos);
	}
	$("#game").mousemove(_ftce.onMove);

	_ftce.run = game.draw = function() {
		_speed += _speed * 0.0001;
		//draw walls
		_leftWall.draw(_xPos, _speed);
		_rightWall.draw(_xPos, _speed);
		var newPos = _wallPos + _speed;
		if(newPos > _leftConfig.blockSize.y)
			_wallPos = newPos % _leftConfig.blockSize.y;
		else
			_wallPos += _speed;
		
		_junkFromAbove.update(_speed, _xPos, _leftWall.innerEdge, _rightWall.innerEdge);
		_junkFromBelow.update(_speed, _xPos, _leftWall.innerEdge, _rightWall.innerEdge);
	};

	
	return _ftce;
};