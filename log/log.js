var currentLoc = 0;
var loggerHeight = 200;
var open = true;
var filterStrength = 20; //higher value smooths out reporting
var frameTime = 0;
var lastLoop = new Date;
var thisLoop;

$(function ()
{
	$('body').append("<div id='logger'></div>");
	var logger = $('#logger').append("<div id='title' onclick='hideLog()'>LOGGER<span id='fps'></span><span onclick='killLog()' id='kill'>CLOSE</span></div>");
	log("--- log ---");
	hideLog();
	initFPS();
});

function initFPS()
{
	var onEachFrame;
	var requestFrame;
	if (window.webkitRequestAnimationFrame)//Chrome
		requestFrame = webkitRequestAnimationFrame;
	else if(window.requestAnimationFrame)
		requestFrame = requestAnimationFrame;
	
	if (requestFrame) {//this is Chrome only right now
			onEachFrame = function(cb) {
				var _cb = function() { cb(); requestFrame(_cb); }
				_cb();
			};
	} else {
		onEachFrame = function(cb) {
			setInterval(cb, 1000 / 60);
		}
	}

	window.onEachFrame = onEachFrame;
	window.onEachFrame(updateFPS);
	setInterval(function(){
		$("#fps").html((1000/frameTime).toFixed(1) + " fps");
	}, 1000);
}

function updateFPS()
{
	thisLoop = new Date;
	var thisFrameTime = thisLoop - lastLoop;
	frameTime += (thisFrameTime - frameTime) / filterStrength;
	lastLoop = thisLoop;
}

function log(param)
{
	var logs = "<ul class='logs'><li>" + String(param) + "</li>";
	
	for(var i = 1; i < arguments.length; i++)
		logs += "<li>" + arguments[i] + "</li>";
		
	logs += "</ul>";
	$('#logger').prepend("<div class='log'><span>" + currentLoc + "</span>" + logs + "</div>");
	window.console.log(currentLoc + "]] " + logs);
	currentLoc++;
}

function hideLog() 
{
	if(open == false) {
		open = true;
		$("#logger").addClass("open");
	} else {
		open = false;
		$("#logger").removeClass("open");
	}
}

function killLog()
{
	$("#logger").hide();
}